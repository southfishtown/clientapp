![](https://gitlab.com/southfishtown/clientapp/raw/master/Capture.PNG)

# Intro
This macOS app is the client app that transfers an image named "nebula.jpg" to the server.

# Tags
objectivec, Storyboard, image transfering, Pod

# Referenced packages
https://github.com/robbiehanson/CocoaAsyncSocket

https://github.com/6david9/ImageTransfer

https://github.com/y-natsui/CocoaAsyncSocket-Server

https://github.com/y-natsui/CocoaAsyncSocket-Client