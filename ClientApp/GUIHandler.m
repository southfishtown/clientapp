//
//  GUIHandler.m
//  ClientApp
//
//  Created by Pu Zhao on 2019/11/1.
//  Copyright © 2019 Pu Zhao. All rights reserved.
//

#import "GUIHandler.h"

#define SENDING_TAG 10000

@implementation GUIHandler{
    NSString *imageName;
    GCDAsyncSocket *asyncSocket;
}

-(id) init{
    self = [super init];
    
    if(self){
        //Send nebula.jpg to server
        imageName = @"nebula.jpg";
        
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:imageName ofType:@""];
        self.imageURL = [[NSURL alloc] initFileURLWithPath:imagePath];
        self.labelText=@"";
        self.buttonText = @"Connect Server";
        self.hostIp = @"192.168.1.13";
        
        self.imageToSend = [[NSImage alloc] initWithContentsOfURL:self.imageURL];
        
        asyncSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
        
        self.isServerConnected = false;
    }
    return self;
}

-(void) dealloc{
    [asyncSocket setDelegate:nil];
    [asyncSocket disconnect];
}

#pragma mark - Connect/Disconnect Server
- (IBAction)ContactServer:(id)sender {
    if(self.isServerConnected == true){
        [self DisconnectServer];
    }else{
        [self ConnectServer];
    }
}

-(void) ConnectServer{
    NSError *err = nil;
    uint16_t port = 20000;
    
    if(![asyncSocket connectToHost:self.hostIp onPort:port withTimeout:5 error:&err]){
        self.labelText =[NSString stringWithFormat:@"Unable to connect due to invalid configuration: %@",err];
    }else{
        self.labelText = [NSString stringWithFormat:@"Connecting to %@ on port %hu", self.hostIp, port];
    }
}

-(void) DisconnectServer{
    self.buttonText = @"Connect Server";
    self.isServerConnected = false;
    self.labelText = @"Disconnected";
    [asyncSocket disconnect];
}

#pragma mark - Prepare image data and send it
- (IBAction)SendPicture:(id)sender {
    if(!self.isServerConnected) return;
    //NSImage to NSData
    NSData * data = [self.imageToSend TIFFRepresentation];
    //Prepare data by appeding its length
    NSData * sendingData = [self prepareData:data];
    //Write data with sending tag
    [asyncSocket writeData:sendingData withTimeout:-1 tag:SENDING_TAG];
}

-(NSData* ) prepareData:(NSData* )data{
    //Prepare data as its length and the actual image data
    NSUInteger len = [data length];
    NSMutableData *sendingData = [[NSMutableData alloc] init];
    [sendingData appendBytes:&len length:sizeof(NSUInteger)];
    [sendingData appendData:data];
    return sendingData;
}

#pragma mark - Protocol GCDAsyncSocket
-(void) socket:(GCDAsyncSocket*)sock didConnectToHost:(NSString *) host port:(uint16_t) port{
    self.isServerConnected = true;
    self.buttonText = @"Disconnect";
    self.labelText = [NSString stringWithFormat:@"Client is connected to server. Host: %@ Port: %hu", host, port];
}

-(void) socket:(GCDAsyncSocket*) sock didWriteDataWithTag:(long)tag{
    self.labelText = [NSString stringWithFormat:@"Socket writes data with tag: %ld", tag];
}

-(void) socket:(GCDAsyncSocket*) sock didWritePartialDataOfLength:(NSUInteger)partialLength tag:(long)tag{
    self.labelText = [NSString stringWithFormat:@"Socket writes data with tag: %ld\npartialLength: %lu\n", tag, partialLength];
}

-(NSTimeInterval) socket:(GCDAsyncSocket*) sock shouldTimeoutReadWithTag:(long)tag elapsed:(NSTimeInterval)elapsed bytesDone:(NSUInteger)length{
    self.labelText = [NSString stringWithFormat:@"Timeout:%lu BytesDone:%lu", tag, (unsigned long)length];
    return -1;
}

-(NSTimeInterval) socket:(GCDAsyncSocket* ) sock shouldTimeoutWriteWithTag:(long) tag elapsed:(NSTimeInterval)elapsed bytesDone:(NSUInteger)length{
    self.labelText = [NSString stringWithFormat:@"Timeout:%lu BytesDone:%lu", tag, (unsigned long)length];
    return -1;
}

@end
