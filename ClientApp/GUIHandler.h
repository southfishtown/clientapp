//
//  GUIHandler.h
//  ClientApp
//
//  Created by Pu Zhao on 2019/11/1.
//  Copyright © 2019 Pu Zhao. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "GCDAsyncSocket.h"

NS_ASSUME_NONNULL_BEGIN

@interface GUIHandler : NSObject<GCDAsyncSocketDelegate>{
    
}

@property NSURL* imageURL;
@property NSString* hostIp;
@property NSString* labelText;
@property NSString* buttonText;
@property NSImage* imageToSend;
@property bool isServerConnected;

- (IBAction)ContactServer:(id)sender;
- (IBAction)SendPicture:(id)sender;

-(void) ConnectServer;
-(void) DisconnectServer;

@end

NS_ASSUME_NONNULL_END
