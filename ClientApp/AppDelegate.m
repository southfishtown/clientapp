//
//  AppDelegate.m
//  ClientApp
//
//  Created by Pu Zhao on 2019/11/1.
//  Copyright © 2019 Pu Zhao. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
